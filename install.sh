#!/bin/bash
set -e

: ${NAME:=$1}

apt install -fy debhelper

cd "$NAME"
for package in $(dh_listpackages); do
    apt install -fy ../build-area/"$package"_*.deb
done
