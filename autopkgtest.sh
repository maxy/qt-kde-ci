#!/bin/bash
set -e

: ${NAME:=$1}
: ${SOURCE_NAME:=$NAME}

apt install -fy autopkgtest python3-debian adduser devscripts

DSC_FILE="$(ls build-area/"$SOURCE_NAME"_*.dsc)"

if ! dscextract "$DSC_FILE" debian/tests/control > /dev/null; then
    echo "No autopkgtests"
    exit 0
fi

addgroup --system debci
adduser --system \
    --home /usr/share/debci \
    --shell /bin/sh \
    --disabled-password \
    --ingroup debci \
    debci

mkdir "build-area/$NAME.autopkgtest" || true
autopkgtest \
    --user=debci \
    --output-dir="build-area/$NAME.autopkgtest" \
    build-area/"$SOURCE_NAME"_*.changes -- null
