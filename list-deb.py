#!/usr/bin/python3
# -*- coding: utf-8 -*-

#  Copyright © 2018 Maximiliano Curia <maxy@gnuservers.com.ar>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

'''
List deb packages from a .changes file.
'''

import argparse
import logging
import os

import debian.deb822 as deb822


def process_options():
    kw = {
        'format': '[%(levelname)s] %(message)s',
    }
    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument('--debug', action='store_true')
    arg_parser.add_argument('--changes-file',
                            default=os.environ.get('CHANGES_FILE', ''))
    args = arg_parser.parse_args()

    if args.debug:
        kw['level'] = logging.DEBUG

    logging.basicConfig(**kw)

    return args


def main():
    args = process_options()
    filename = args.changes_file
    dirname = os.path.dirname(filename)

    with open(filename) as changes_file:
        changes = deb822.Changes(changes_file)
        for item in changes["files"]:
            if item["name"].endswith(".deb"):
                print(os.path.join(dirname, item["name"]))


if __name__ == '__main__':
    main()
