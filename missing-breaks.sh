#!/bin/bash
set -e

: ${NAME:=$1}
: ${SOURCE_NAME:=$NAME}

apt install -fy apt-file python3-debian python3-junit.xml
apt update

./check_for_missing_breaks_replaces.py \
    -o build-area/"$NAME".missing-breaks.xml \
    --changes-file build-area/"$SOURCE_NAME"_*.changes
