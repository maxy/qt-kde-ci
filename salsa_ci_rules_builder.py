#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# salsa_ci_rules_builder, salsa ci rules builder
#  Copyright © 2018 Maximiliano Curia <maxy@gnuservers.com.ar>

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

'''
Generates a gitlab-ci yaml file from a set of Debian package
source directories.

This is intended for group of packages that are part of a release group, which
means that in order to test the packages you'll need to build all it's
dependencies internal to the group.
'''

from __future__ import print_function

import argparse
import collections
import enum
import logging
import re
import os
import sys
import yaml

import debian.deb822 as deb822


def process_options():
    kw = {
        'format': '[%(levelname)s] %(message)s',
    }

    arg_parser = argparse.ArgumentParser(description=__doc__,
                                         fromfile_prefix_chars='@')
    arg_parser.add_argument('package_dirs',
                            help='Debian package directories',
                            nargs='+')
    arg_parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                            help='Output file', default=sys.stdout)
    arg_parser.add_argument('--debug', action='store_true')
    args = arg_parser.parse_args()

    if args.debug:
        kw['level'] = logging.DEBUG

    logging.basicConfig(**kw)

    return args


class Package(object):
    ''' Information about the package '''

    def __init__(self, path):
        ''' path: Package directory '''

        assert(os.path.isdir(path))
        self.path = path

    @property
    def name(self):
        return os.path.split(self.path)[-1]

    @property
    def control(self):
        control_file = os.path.join(self.path, 'debian/control')

        with open(control_file) as f:
            return list(deb822.Deb822.iter_paragraphs(f))

    @property
    def tests_control(self):
        control_file = os.path.join(self.path, 'debian/tests/control')

        if not os.path.isfile(control_file):
            # No tests
            return []

        with open(control_file) as f:
            return list(deb822.Deb822.iter_paragraphs(f))

    @property
    def copyright(self):
        copyright_filename = os.path.join(self.path, 'debian/copyright')
        with open(copyright_filename) as f:
            return list(deb822.Deb822.iter_paragraphs(f))

    @property
    def build_depends(self):
        build_deps = []
        source_control = self.control[0]
        build_deps.append(source_control.get('Build-Depends'))
        build_deps.append(source_control.get('Build-Depends-Indep'))
        return ', '.join(x for x in build_deps if x)

    @property
    def runtime_depends(self):
        deps = []
        binary_controls = self.control[1:]
        for binary_control in binary_controls:
            deps.append(binary_control.get('Depends'))
        return ', '.join(x for x in deps if x)

    @property
    def recommends(self):
        deps = []
        binary_controls = self.control[1:]
        for binary_control in binary_controls:
            deps.append(binary_control.get('Recommends'))
        return ', '.join(x for x in deps if x)

    @property
    def tests_depends(self):
        tests_deps = []
        additionals = set()
        for test in self.tests_control:
            depends = test.get('Depends', '')
            depends_list = re.split(r'\s*,\s*', depends)
            if '@' in depends_list:
                depends_list.remove('@')
                additionals.add('runtime')
            if '@builddeps@' in depends_list:
                depends_list.remove('@builddeps@')
                additionals.add('build')
            restrictions = test.get('Restrictions', '')
            restrictions_list = re.split(r'\s*,\s*', restrictions)
            if 'needs-recommends' in restrictions_list:
                additionals.add('recommends')
            tests_deps.extend(depends_list)
        if 'runtime' in additionals:
            tests_deps.append(self.runtime_depends)
            if 'recommends' in additionals:
                tests_deps.append(self.recommends)
        if 'build' in additionals:
            tests_deps.append(self.build_depends)

        return ', '.join(x for x in tests_deps if x)

    @property
    def list_packages(self):
        'A simple dh_listpackages'
        packages = []
        for part in self.control:
            package_name = part.get('Package')
            if package_name:
                packages.append(package_name)
        return packages

    @property
    def description(self):
        for block in self.control:
            if 'Description' in block:
                return block['Description']
        return ''

    @property
    def short_description(self):
        return self.description.split('\n', 2)[0]

    @property
    def upstream_metadata(self):
        ''' Obtain the contents of the debian/upstream/metadata file '''
        metadata_filename = os.path.join(self.path, 'debian/upstream/metadata')
        if not os.path.exists(metadata_filename):
            return
        with open(metadata_filename) as f:
            return yaml.load(f)

    @property
    def source_name(self):
        ''' Source package name '''
        return self.control[0].get('Source')

    @property
    def upstream_vcs(self):
        ''' Obtain upstream_vcs.

    Use the information from the debian/upstream/metadata file(DEP12),
    if that's not available, use the Source field in the debian/copyright
    (if it's in DEP5 format), and fallback to use the Homepage field
    from the control file.
        '''

        upstream_metadata = self.upstream_metadata
        if upstream_metadata:
            vcs = self.upstream_metadata.get('Repository')
            if vcs:
                return vcs
        copyright = self.copyright[0]
        if copyright and copyright.get('Source'):
            return copyright.get('Source')
        control = self.control[0]
        return control.get('Homepage')

    @property
    def vcs_git(self):
        control = self.control[0]
        return control.get('Vcs-Git')

    def __repr__(self):
        return '(Package path:{} )'.format(self.path)

    def __str__(self):
        return self.path


class OrderedSet(deb822.OrderedSet, collections.MutableSet):

    discard = deb822.OrderedSet.remove

    def __reversed__(self):
        # Return an iterator of items in the order they were added
        return reversed(self.__order)

    def pop(self, last=True):
        if not self:
            raise KeyError('pop from an empty set')
        key = self.__order.pop() if last else self.__order.pop(0)
        self.__set.remove(key)
        return key

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, ', '.join(self))

    def __eq__(self, other):
        if isinstance(other, deb822.OrderedSet):
            return len(self) == len(other) and list(self) == list(other)
        return set(self) == set(other)


# Graph node
Node = collections.namedtuple('Node', ['input', 'output'])


class GraphError(Exception):
    pass


class WalkEnum(enum.Enum):
    DEPTH = 1
    BREADTH = 2


class TSortGraph(object):

    '''Graph for topological sorts.'''

    def __init__(self, keys, get_inputs, walk=WalkEnum.DEPTH):
        '''
        :keys: sequence of hashable items.
        :get_inputs: function of key that returns the list of inputs that the
                     node depends on.
        '''
        self.nodes = collections.defaultdict(
            lambda: Node(OrderedSet(), OrderedSet())
        )
        self.ready = collections.deque()
        self._done = set()
        visited = set()

        node_keys = OrderedSet(keys)
        self.walk = walk

        for key in node_keys:
            if key in visited:
                continue
            inputs = get_inputs(key)
            # Reduce the inputs to the set of nodes we are working with
            inputs &= node_keys
            self.nodes[key].input.extend(inputs)
            for input in inputs:
                self.nodes[input].output.add(key)
            if not inputs:
                self.ready.append(key)
            visited.add(key)

    def done(self, key):
        '''Remove key from the inputs of the depending nodes'''
        self._done.add(key)

        ready = []
        for child in self.nodes[key].output:
            self.nodes[child].input.remove(key)
            # if there are no more dependencies, the element is ready
            if not self.nodes[child].input:
                ready.append(child)
        if self.walk == WalkEnum.DEPTH:
            for child in reversed(ready):
                # This tries to follow a "dfs" walk this way for item we can
                # check the inheritance in the following order
                # item/sid group/sid /sid item group
                self.ready.appendleft(child)
        else:
            for child in ready:
                self.ready.append(child)
        return self.ready

    def __repr__(self):
        items = ('{key}: in={node.inputs}, out={node.outputs}'.format(
            key=key, node=node) for key, node in self.nodes.items())
        return '{}({})'.format(self.__class__.__name__, ', '.join(items))

    def sort_generator(self, skip=None):
        '''Topological sort as a generator

        :skip: is a set of values that are not going to be considered as done.
        '''
        while self.ready:
            key = self.ready.popleft()
            yield key
            if skip and key in skip:
                continue
            self.done(key)

        if len(self._done) != len(self.nodes) and not skip:
            raise GraphError('Not a DAG?: done {}, ready {}, graph {}'.format(
                self._done, self.ready, self))


def obtain_packages(dirs):
    packages = {}
    for directory in dirs:
        package = Package(directory)
        packages[package.name] = package
    return packages


def package_relations(binaries, dependencies):
    '''Process dependencies against binaries

    :binaries: is a dictionary that maps binaries to the package_name
               that generates it.
    :dependencies: is a dpkg depends string
    :returns: a mapping, of the package_names that generate the needed
              binaries, and the binaries that cause the dependency as the
              value.
    '''
    bin_deps = set()
    rels = deb822.PkgRelation.parse_relations(dependencies)
    for or_part in rels:
        for part in or_part:
            bin_deps.add(part['name'])
    internal_dep = {}
    for dep in bin_deps:
        if dep in binaries:
            internal_dep.setdefault(binaries[dep], []).append(dep)
    return internal_dep


def process_dependencies(packages):

    def init_relation(relations, name):
        if name not in relations:
            relations[name] = {}
        if 'reverse_build' not in relations[name]:
            relations[name]['reverse_build'] = []
        return relations[name]

    binaries = {}
    for name, package in packages.items():
        for binary in package.list_packages:
            if binary in binaries:
                logging.error(
                    'ERROR: The package {} seems to be produced by two '
                    'or more source packages: {}, {}'.format(
                        binary,
                        binaries[binary],
                        name))
            else:
                binaries[binary] = name
    relations = {}
    for name, package in packages.items():
        relation = init_relation(relations, name)

        relation['build'] = \
            package_relations(binaries, package.build_depends)
        relation['runtime'] = \
            package_relations(binaries, package.runtime_depends)
        relation['test'] = \
            package_relations(binaries, package.tests_depends)

        for dependency in relation['build']:
            dep_rel = init_relation(relations, dependency)
            dep_rel['reverse_build'].append(name)

    return relations


def prepare_graph(relations):
    def _get_inputs(package_name):
        return relations[package_name]['build']
    return TSortGraph(relations.keys(), _get_inputs, walk=WalkEnum.BREADTH)


CI_DEFAULT = '''\
.job_template: &template
    image: debian:sid
    tags:
        - gitlabrunner2
    variables:
        DEBIAN_FRONTEND: noninteractive
        LANG: C.UTF-8
        CMD_PREFIX: eatmydata
    before_script:
        - echo force-unsafe-io > /etc/dpkg/dpkg.cfg.d/disable_fsync
        - echo 'Apt::Get::Assume-yes "true";' > /etc/apt/apt.conf.d/99assume_yes
        - apt update
        - apt install -fy eatmydata
        - $CMD_PREFIX apt upgrade -fy
'''

BUILD_JOB_TEMPLATE = '''\
build:{name}:
    <<: *template
    stage: {stage}
    script:
        - set -e
        - export NAME="{name}"
        - export SOURCE_NAME="{source_name}"
        - export DEBIAN_GIT="{debian_git}"
        - export UPSTREAM_GIT="{upstream_git}"
        - $CMD_PREFIX ./prepare.sh
        - $CMD_PREFIX ./local-packages.sh
        - $CMD_PREFIX ./checkout.sh
        - $CMD_PREFIX ./build.sh
    artifacts:
        paths:
            - {name}
            - build-area
'''


def build_stages(packages, relations):

    graph = prepare_graph(relations)
    skip = set()
    layer = 0
    stages = collections.OrderedDict()
    while graph.ready:
        # print('LAYER {}: {}'.format(layer, graph.ready))
        stage_name = 'build_{}'.format(layer)
        stage = set()
        stages[stage_name] = stage
        for name in graph.sort_generator(skip=skip):
            skip.add(name)
            stage.add(name)

        for name in stage:
            skip.remove(name)
            graph.done(name)
        layer += 1

    stages['test'] = set()

    return stages


def prepare_dependencies(name, _type, relations):
    dependencies = []
    for dependency in relations[name][_type]:
        dependencies.append(
            '        - build:{dependency}\n'.format(
                dependency=dependency)
        )
    if _type in {'runtime', 'test'} and name not in relations[name][_type]:
        dependencies.append(
            '        - build:{dependency}\n'.format(
                dependency=name)
        )

    return ''.join(dependencies)


def build_job(stage_name, package, relations):
    job = BUILD_JOB_TEMPLATE.format(
        debian_git=package.vcs_git,
        name=package.name,
        source_name=package.source_name,
        stage=stage_name,
        upstream_git=package.upstream_vcs,
    )
    build_deps = prepare_dependencies(package.name, 'build', relations)
    if build_deps:
        job += '    dependencies:\n' + build_deps
    return job


TEST_INSTALL_TEMPLATE = '''\
test:install:{name}:
    <<: *template
    stage: test
    script:
        - set -e
        - export NAME="{name}"
        - $CMD_PREFIX ./local-packages.sh
        - $CMD_PREFIX ./install.sh
    dependencies:
{runtime_deps}'''
TEST_LINTIAN_TEMPLATE = '''\
test:lintian:{name}:
    <<: *template
    stage: test
    script:
        - set -e
        - export NAME="{name}"
        - export SOURCE_NAME="{source_name}"
        - $CMD_PREFIX ./lintian.sh
    artifacts:
      paths:
        - build-area/{name}.lintian
    dependencies:
        - build:{name}
'''
TEST_AUTOPKGTEST_TEMPLATE = '''\
test:autopkgtest:{name}:
    <<: *template
    stage: test
    script:
        - set -e
        - export NAME="{name}"
        - export SOURCE_NAME="{source_name}"
        - $CMD_PREFIX ./local-packages.sh
        - $CMD_PREFIX ./autopkgtest.sh
    artifacts:
        paths:
            - build-area/{name}.autopkgtest
    dependencies:
{test_deps}'''
TEST_MISSING_BREAKS_TEMPLATE = '''\
test:missing-breaks:{name}:
    <<: *template
    stage: test
    script:
        - set -e
        - export NAME="{name}"
        - $CMD_PREFIX ./missing-breaks.sh
    artifacts:
        paths:
            - build-area/{name}.missing-breaks.xml
    dependencies:
        - build:{name}
'''
TEST_RC_BUGS_TEMPLATE = '''\
test:check-rc-bugs:{name}:
    <<: *template
    stage: test
    script:
        - set -e
        - $CMD_PREFIX ./check-rc-bugs.sh
    artifacts:
        paths:
            - build-area/{name}.check-rc-bugs.xml
    dependencies:
        - build:{name}
'''


def test_jobs(package, relations):
    jobs = []

    runtime_deps = prepare_dependencies(package.name, 'runtime', relations)
    test_deps = prepare_dependencies(package.name, 'test', relations)

    install_job = TEST_INSTALL_TEMPLATE.format(
        name=package.name,
        source_name=package.source_name,
        runtime_deps=runtime_deps,
        test_deps=test_deps,
    )
    jobs.append(install_job)

    lintian_job = TEST_LINTIAN_TEMPLATE.format(
        name=package.name,
        source_name=package.source_name,
        runtime_deps=runtime_deps,
        test_deps=test_deps,
    )
    jobs.append(lintian_job)

    autopkgtest_job = TEST_AUTOPKGTEST_TEMPLATE.format(
        name=package.name,
        source_name=package.source_name,
        runtime_deps=runtime_deps,
        test_deps=test_deps,
    )
    jobs.append(autopkgtest_job)

    missing_breaks_job = TEST_MISSING_BREAKS_TEMPLATE.format(
        name=package.name,
        source_name=package.source_name,
        runtime_deps=runtime_deps,
        test_deps=test_deps,
    )
    jobs.append(missing_breaks_job)

    rc_bugs_job = TEST_RC_BUGS_TEMPLATE.format(
        name=package.name,
        source_name=package.source_name,
        runtime_deps=runtime_deps,
        test_deps=test_deps,
    )
    jobs.append(rc_bugs_job)

    return jobs


def dump(stages, packages, relations, output):

    print(CI_DEFAULT, file=output)

    print('stages:\n' +
          ''.join(
              ('  - {}\n'.format(stage)
               for stage in stages)
          ),
          file=output)

    for stage_name, names in stages.items():
        for name in names:
            package = packages[name]
            job = build_job(stage_name, package, relations)
            print(job, file=output)

    for name, package in packages.items():
        jobs = test_jobs(package, relations)
        for job in jobs:
            print(job, file=output)


def main():
    options = process_options()

    packages = obtain_packages(options.package_dirs)
    relations = process_dependencies(packages)
    stages = build_stages(packages, relations)
    dump(stages, packages, relations, options.output)
    # group_tree, groups = group_packages(packages, args.basedir)
    # projects = prepare_projects(group_tree, packages_relations,
    #                             args.local_repository,
    #                             args.local_vcs)
    # dump_projects(projects, args.output)


if __name__ == "__main__":
    main()
