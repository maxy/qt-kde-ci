#!/bin/bash
set -e

apt install -fy apt-utils

cd build-area
apt-ftparchive packages . > Packages
apt-ftparchive release . > Release

echo "deb [trusted=yes] file:$(pwd) ./" \
    > /etc/apt/sources.list.d/local-packages.list
apt update
