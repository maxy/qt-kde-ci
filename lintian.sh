#!/bin/bash
set -e

: ${NAME:=$1}
: ${SOURCE_NAME:=$NAME}

apt install -fy lintian

lintian -I --pedantic --show-overrides --allow-root \
    ./build-area/"$SOURCE_NAME"_*.changes | \
    tee build-area/"$NAME".lintian
