#!/bin/bash
set -e

: ${NAME:=$1}

cd "$NAME"
apt build-dep -fy .
gbp buildpackage \
    --git-upstream-tag='v%(version)s' \
    --git-export-dir=../build-area \
    --git-tarball-dir=../build-area \
    --git-overlay \
    --git-compression=xz \
    --git-builder='debuild -i -I -us -uc'
