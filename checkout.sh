#!/bin/bash
set -e

: ${NAME:=$1}
: ${DEBIAN_GIT:=https://salsa.debian.org/qt-kde-team/kde/$NAME.git}
: ${UPSTREAM_GIT:=https://anongit.kde.org/$NAME.git}

git clone "$DEBIAN_GIT" "$NAME"
cd "$NAME"
git remote add upstream "$UPSTREAM_GIT"
git fetch --all --tags
