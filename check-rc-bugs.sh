#!/bin/bash
set -e

: ${NAME:=$1}
: ${SOURCE_NAME:=$NAME}

apt install -fy python3-debian python3-debianbts python3-junit.xml
apt update

./check_rc_bugs.py \
    -o build-area/"$NAME".check-rc-bugs.xml \
    --changes-file build-area/"$SOURCE_NAME"_*.changes
